<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Edit Task</title>
</head>

<body>
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        @if(auth()->check())
        <a class="navbar-brand" href="#">
            <h1>Hello, {{ auth()->user()->name }}</h1>
        </a>
        <form id="logoutForm">
            @csrf
            <button type="submit" class="btn btn-primary">Logout</button>
        </form>
        @else
        <script>
        window.location = "{{ route('login') }}";
        </script>
        @endif
    </nav>
    <div class="container">
        <h2>Edit Task</h2>
        <form id="editTaskForm">
            <div class="form-group">
                <label for="editTitle">Title:</label>
                <input type="text" class="form-control" id="editTitle" name="title" required>
            </div>
            <div class="form-group">
                <label for="editDescription">Description:</label>
                <textarea class="form-control" id="editDescription" name="description" rows="3" required></textarea>
            </div>
            <div class="form-group">
                <label for="editStatus">Status:</label>
                <select class="form-control" id="editStatus" name="status" required>
                    <option value="incomplete">Incomplete</option>
                    <option value="complete">Complete</option>
                </select>
            </div>
            <input type="hidden" id="editTaskId" name="taskId">

            <button type="submit" class="btn btn-primary">Save Changes</button>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>

    <script>
    $(document).ready(function() {
        var taskId = window.location.pathname.split('/').pop();
        fetchTaskDetails(taskId);

        $("#editTaskForm").submit(function(e) {
            e.preventDefault();
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var headers = {
                'X-CSRF-TOKEN': csrfToken,
            };


            $.ajax({
                type: "PATCH",
                headers: headers,
                url: "/api/tasks/" + taskId,
                data: $("#editTaskForm").serialize(),
                success: function(response) {
                    window.location.href = "/";
                },
                error: function(xhr, status, error) {
                    console.error("Error updating task details:", error);
                },
            });
        });

        function fetchTaskDetails(taskId) {
            $.ajax({
                type: "GET",
                url: "/api/tasks/" + taskId,
                success: function(response) {
                    $("#editTitle").val(response.task.title);
                    $("#editDescription").val(response.task.description);
                    $("#editStatus").val(response.task.status);

                    $("#editTaskId").val(taskId);
                },
                error: function(xhr, status, error) {
                    console.error("Error fetching task details:", error);
                },
            });
        }
    });
    </script>
</body>

</html>