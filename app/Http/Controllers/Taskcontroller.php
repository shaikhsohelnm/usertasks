<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;


class Taskcontroller extends Controller
{
    //
    public function newtask(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255|unique:task,title',
            'description' => 'required|string',
            'status' => 'required|string',
        ]);

        $task = Task::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'usertask_id' => Auth::user()->id,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'User created successfully',
            'user' => $task,
        ]);
    }
public function gettask(Request $request)
{
    $statusFilter = $request->input('status');
    $perPage = 5; 

    if (Auth::user()->usertype === 'admin') {
            $tasks = ($statusFilter)
            ? Task::where('status', $statusFilter)
            ->with('user:id,name')  
            ->paginate($perPage)
            : Task::with('user:id,name') 
            ->paginate($perPage);

        return response()->json([
            'status' => 'success',
            'tasks' => $tasks,
        ]);
    } else if (Auth::user()->usertype === 'user') {
        $tasks = ($statusFilter)
            ? Task::where('usertask_id', Auth::user()->id)
                  ->where('status', $statusFilter)
                  ->with('user:id,name')
                  ->paginate($perPage)
            : Task::where('usertask_id', Auth::user()->id)
                  ->with('user:id,name')
                  ->paginate($perPage);

        return response()->json([
            'status' => 'success',
            'tasks' => $tasks,
        ]);
    } else {
        return response()->json([
            'status' => 'error',
            'message' => 'User not found',
        ]);
    }
}

    public function destroy(Task $task)
    {
        try {
            $task->delete();

            return response()->json(['message' => 'Task deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error deleting task'], 500);
        }
    }
    
        public function updateStatus($id)
    {
        // $this->validate($request, [
        //     'status' => 'required|in:complete,incomplete',
        // ]);

        $task = Task::findOrFail($id);

        $task->status = $task->status === 'complete' ? 'incomplete' : 'complete';
        $task->save();

        return response()->json(['message' => 'Task status updated successfully']);
    }

    public function getTaskDetails($id)
    {
        try {
            $task = Task::findOrFail($id);

            return response()->json([
                'status' => 'success',
                'task' => $task,
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Task not found'], 404);
        }
    }
    public function updateTask(Request $request, $id)
    {
        try {
            $task = Task::findOrFail($id);

            $request->validate([
                'title' => 'required|string|max:255|unique:task,title,' . $id,
                'description' => 'required|string',
                'status' => 'required|string|in:complete,incomplete',
            ]);

            $task->title = $request->input('title');
            $task->description = $request->input('description');
            $task->status = $request->input('status');
            $task->save();

            return response()->json(['message' => 'Task updated successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error updating task'. $e], 500);
        }
    }

}